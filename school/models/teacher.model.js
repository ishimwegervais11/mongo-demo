const mongoose = require('mongoose')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const config = require('config')

const teacherSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        require:true,
        unique:true

    },
    password:{
        type:String,
        required:true
    },
    isAdmin:{
        type:Boolean,
        default:false
    },
    schoolId:{
        type:String,
        required:true
    }
})

teacherSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id:this._id,name:this.name,email:this.email,schoolId:this.schoolId,isAdmin:this.isAdmin},
        config.get('jwtPrivateKey'))

        return token
    
}

 const Teacher = mongoose.model('Teacher',teacherSchema)

 function validateTeacher(teacher){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        password:Joi.string().max(255).min(3).required(),
        isAdmin:Joi.string(),
        schoolId:Joi.string().required()
    }

    return Joi.validate(teacher,schema)
 }

 module.exports.Teacher = Teacher
 module.exports.validateTeacher = validateTeacher
