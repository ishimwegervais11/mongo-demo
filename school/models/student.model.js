const mongoose = require('mongoose')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const config = require('config')

const studentSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    gender:{
        type:String,
        required:true

    },
    age:{
        type:Number,
        required:true
    },
    schoolId:{
        type:String,
        required:true
    }
})


 const Student = mongoose.model('Student',studentSchema)

 function validateStudent(student){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        gender:Joi.string().max(255).min(3).required(),
        age:Joi.number().required(),
        schoolId:Joi.string().required()
    }

    return Joi.validate(student,schema)
 }

 module.exports.Student = Student
 module.exports.validateStudent = validateStudent
