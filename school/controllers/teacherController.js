const express = require('express')
const hashPassword = require('../utils/hash')
const _ = require('lodash')
const {Teacher,validateTeacher} = require('../models/teacher.model')
let router = express.Router()
const authMidleware = require('../middleware/auth')
const {School}=require('../models/school.model')
const admin = require('../middleware/admin')


router.get('/', async (req,res) => {
    const teacher = await Teacher.find().sort({name:1})

    return res.send(teacher)
})


router.get('/admin', async (req,res) => {
    const teacher = await Teacher.find({isAdmin:true})
    return res.send(teacher).status(200)
})

router.get('/nonadmin', async (req,res) => {
    const teacher = await Teacher.find({isAdmin:false})
    return res.send(teacher).status(200)
})


router.get('/:_id', async (req,res) => {
    const teacher = await Teacher.find({_id:req.params._id})
    return res.send(teacher).status(200)
})



router.post('/',[authMidleware,admin], async (req,res) => {
    const {error} = validateTeacher(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let teacher = await Teacher.findOne({email:req.body.email})
    if(teacher) return res.send('Teacher already there').status(400)

    school = await School.find({_id:req.body.schoolId})
    if(!school) return res.send(`We dont have such school with ${req.body.schoolId} in our system`).status(404)


    teacher = new Teacher(_.pick(req.body,['name','email', 'password','isAdmin','schoolId']))
    const hashed = await hashPassword(teacher.password)
    teacher.password = hashed
    await teacher.save()
    return res.send(_.pick(teacher,['_id','name','email','isAdmin','schoolId']))
})

router.put('/', [authMidleware,admin], async (req,res) => {
    const {error} = validateTeacher(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    school = await School.findOne({_id:req.body.schoolId})
    if(!school) return res.send(`We dont have such school with ${req.body.schoolId} in our system`).status(404)


    Teacher.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(teacher => res.send(teacher).status(200))
    .catch( err => res.send(err).status(404))
})

router.delete('/:id', [authMidleware,admin], async (req,res) => {

    Teacher.findOneAndRemove({id:req.params.id})
    .then(teacher => res.send(teacher).status(200))
    .catch( err => res.send(err).status(400))
})

router.get('/admin', (req, res) => {
    Teacher.find({isAdmin:true})
        .then(teacher => res.send(teacher))
        .catch(err => res.send(err).status(404));
    });
    router.get('/nonadmin', (req, res) => {
        Teacher.find({isAdmin:false})
            .then(teacher => res.send(teacher))
            .catch(err => res.send(err).status(404));
        });
module.exports = router;