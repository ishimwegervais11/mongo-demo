require('./models/mongodb')
const schoolController = require('./controllers/schoolController')
const teacherController = require('./controllers/teacherController')
const authControllers = require('./controllers/auth')
const studentController = require('./controllers/studentController')
const statiController = require('./controllers/statsControllers')
const config = require('config')
const express = require('express') 
const authMidleware = require('./middleware/auth')


let app = express()
const bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({extended:true}))
app.use(bodyparser.json())

if(!config.get("jwtPrivateKey")){
    console.log("JWT private key is not defined")
    process.exit(1)
}

//welcoming page

app.get('/' ,(req, res) => {
    res.send('Welcome to our Online shop')
})

//set the Controllers path wich will be responding the user
app.use('/api/teachers', teacherController)
app.use('/api/auth',authControllers)
app.use('/api/students',studentController)
app.use('/api/schools', schoolController)
app.use('/api/stats', statiController)


const port = process.env.PORT || 4040
app.listen(port,() => console.log(`listening on port ${port}`))


// sudo kill 'sudo lsof -t -i:9001'