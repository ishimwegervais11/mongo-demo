const Joi = require('joi')
const express = require('express');
const app = express();
app.use(express.json())
let authors = [{
        id: 1,
        name: "Ntwari ",
        email: "ntwaricliberi@gmail.com"
    },
    {
        id: 2,
        name: "liberi ",
        email: "liberi@gmail.com"
    },
    {
        id: 3,
        name: "clarance ",
        email: "clarance@gmail.com"
    },
    {
        id: 4,
        name: "rocky ",
        email: "rocky@gmail.com"
    },
    {
        id: 5,
        name: "Giti ",
        email: "giti@gmail.com"
    },
    {
        id: 6,
        name: "divin ",
        email: "divin@gmail.com"
    },
    {
        id: 7,
        name: "daniel ",
        email: "daniel@gmail.com"
    },
    {
        id: 8,
        name: "keep it up ",
        email: "kepp-it-up@gmail.com"
    }
];

//get all authors
app.get('/api/authors', (req, res) => {
    return res.send(authors)
});
//get author by id
app.get('/api/authors/:id', (req, res) => {
    //look up the author from authors list
    const author = authors.find(c => c.id == req.params.id)
    //if author is not found return 404
    if (!author) return res.status(404).send("author not found")
    //otherwise return the author
    return res.send(author)
});
//insert a new author
app.post('/api/authors', (req, res) => {
    if (!req.body.name) return res.status(400).send("author name is required")
    if (req.body.name.length < 4) return res.status(400).send("Name should be atleast four characters")
    let author = {
        id: authors.length + 1,
        name: req.body.name
    }
    //append the author to list
    authors.push(author)
    return res.status(201).send(author)
});
//create new and validate with joi
app.post('/api/authors/validated', (req, res) => {
    const schema = {
        name: Joi.string().min(3).max(30).required()
    }
    const result = Joi.validate(req.body, schema);
    console.log(result)
    if (result.error) {
        return res.status(400).send(result.error.details[0].message);
    }
    let author = {
        id: authors.length + 1,
        name: req.body.name
    }
    authors.push(author)
    return res.status(201).send(author)
});
//update author
app.put('/api/authors', (req, res) => {
    //look up the author from authors list
    const author = authors.find(c => c.id == req.body.id)
    //if author is not found return 404
    if (!author) return res.status(404).send("author not found")
    //if author if found, validate the author name
    const schema = {
        name: Joi.string().min(3).max(30).required(),
        id: Joi.number().required()
    }
    const result = Joi.validate(req.body, schema);
    // if name not validated return 400
    if (result.error) {
        return res.status(400).send(result.error.details[0].message);
    }
    //otherwise update the author
    author.name = req.body.name
    //then return updted author  
    return res.status(201).send(author)
});
//delete a author
app.delete('/api/authors/:id', (req, res) => {
    //look up the author from authors list
    const author = authors.find(c => c.id == req.params.id)
    //if author is not found return 404
    if (!author) return res.status(404).send("author not found")
    //delete the author
    //else remove the author
    const index = authors.indexOf(author);
    // remove author
    // go to certain and remove one object
    authors.splice(index, 1);
    res.send(authors)
})
const port = 1000
app.listen(port, () => console.log(`Server running on port ${port}`))