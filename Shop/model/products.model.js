const mongoose = require('mongoose')
const Joi = require('joi')


const productSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    price:{
        type:Number,
        require:true,

    },
    description:{
        type:String
    },
    category_id:{
        type:Number,
        required:true
    } 
})

const Product = mongoose.model('Product',productSchema)


function validateProduct(product){
    const schema = {
        name: Joi.string().max(255).min(3).required(),
        price: Joi.number().integer().max(100000000).required(),
        description: Joi.string().max(255).min(20),
        category_id: Joi.number().integer().max(255).required()
    }

    return Joi.validate(product,schema)
 }

 module.exports.Product = Product
 module.exports.validateProduct = validateProduct