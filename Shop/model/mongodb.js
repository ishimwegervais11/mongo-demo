const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/shop', {
    useNewUrlParser:true,
    useUnifiedTopology:true
})
.then(() => console.log('Connected to shop DB succesfully'))
.catch(err => console.log('failed to connect to shop DB',err))

require('./category.model')
require('./products.model')
require('./user.model')
