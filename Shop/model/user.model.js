const mongoose = require('mongoose')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const config = require('config')

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        require:true,
        unique:true

    },
    password:{
        type:String,
        required:true
    },
    isAdmin:{
        type:String,
        required:true,
        default:false
    } 
})

userSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id:this._id,name:this.name,email:this.email},
        config.get('jwtPrivateKey'))

        return token
    
}

 const User = mongoose.model('User',userSchema)

 function validateUser(user){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        password:Joi.string().max(255).min(3).required(),
        isAdmin:Joi.string()
    }

    return Joi.validate(user,schema)
 }

 module.exports.User = User
 module.exports.validateUser = validateUser
