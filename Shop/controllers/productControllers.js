const express = require('express')
const _ = require('lodash')
const {Product,validateProduct} = require('../model/products.model')
const {Category} = require('../model/category.model')
let router = express.Router()


router.get('/', async (req,res) => {
    const product = await Product.find().sort({name:1})
    return res.send(product).status(200)

})

router.get('/name/:name',async (req,res) => {
    const product = await Product.findOne({name:req.params.name})
    return res.send(product).status(200)
})

router.get('/price/:price',async (req,res) => {
    const product = await Product.findOne({price:req.params.price})
    return res.send(product).status(200)
})

router.get('/category/:category',async (req,res) => {
    const product = await Product.find({category_id:req.params.category})
    return res.send(product).status(200)
})

router.post('/',authMidleware async (req,res) => {
    const {error} = validateProduct(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    const isAvailabe = await Category.findOne({id:req.body.category_id})
    if(!isAvailabe) return res.send(`The category you have enterd is incorect we don't have it `).status(404)

    let product = await Product.findOne({name:req.body.name})
    if(product) res.send('Product alread there').status(400)

    product = new Product(_.pick(req.body,['name','price','description','category_id']))
    await product.save()

    return res.send(_.pick(product,['id','name','price','description','category_id']))

})

router.put('/',authMidleware, async (req,res) => {
    const {error} = validateProduct(req.body)
    if(error) return res.send(error.details[0].message)


    Product.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(product => res.send(product).status(200))
    .catch(err => res.send(err).status(404))
    
})

router.delete('/:_id', authMidleware,async (req,res) => {
    Product.findOneAndRemove({_id:req.params._id})
    .then(product => res.send(product).status(200))
    .catch(err => res.send(err).status(404))
})

module.exports = router