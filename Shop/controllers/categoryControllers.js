const express = require('express')
const _ = require('lodash')
const {Category,validateCategory} = require('../model/category.model')
let router = express.Router()

router.get('/', async (req,res) => {
    const category = await Category.find().sort({name:1})

    return res.send(category)
})

router.get('/:id', async (req,res) => {
    const category = await Category.find({id:req.params.id})

    return res.send(category).status(200)
})

router.post('/',authMidleware, async (req,res) => {
    const {error} = validateCategory(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let category = await Category.findOne({name:req.body.name})
    if(category) return res.send('Category already there').status(400)

    category = new Category(_.pick(req.body,['id','name']))
    await category.save()
    return res.send(_.pick(category,['_id','id','name']))
})

router.put('/',authMidleware, async (req,res) => {
    const {error} = validateCategory(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    Category.findOneAndUpdate({id:req.body.id},req.body,{new:true})
    .then(category => res.send(category).status(200))
    .catch( err => res.send(err).status(404))
})

router.delete('/:id',authMidleware, async (req,res) => {

    Category.findOneAndRemove({id:req.params.id})
    .then(category => res.send(category).status(200))
    .catch( err => res.send(err).status(400))
})

module.exports = router;