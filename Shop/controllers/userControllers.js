const hassPassword  = require('../utils/hash')
const _ = require('lodash')
const express = require('express')
const {User,validateUser} = require('../model/user.model')
let router = express.Router()



//get suers
router.get('/', async (req,res) => {
    const users = await User.find().sort({name:1})

    return res.send(users)
})

router.get('/:email',async (req,res) => {
    const users  = await User.find({email:req.params.email})
    return res.send(users)
})

router.post('/',authMidleware, async (req,res) => {
    const {error} = validateUser(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user  = await User.findOne({email:req.body.email})
    if(user) return res.send('User already registered').status(400)

    user=  new User(_.pick(req.body,['name','email','password']))
    // hash passoword
    const hashed = await hassPassword(user.password)
    user.password = hashed
    await user.save()

    return res.send(_.pick(user,['id','name','email']))
})

router.put('/',authMidleware,async (req,res) => {
    const {error} = validateUser(req.body)
    if(error) return send(error.details[0].message).status(400)

    User.findOneAndUpdate({_id:req.body._id},req.body, {new:true})
    .then( user => res.send(user).status(200))
    .catch(err => res.send(err).status(404)  )
})

router.delete('/',authMidleware,async (req,res) => {
    User.findOneAndDelete({_id:req.body._id})
    .then( user => res.send(user).status(200))
    .catch(err => res.send(err).status(404)  )
})


module.exports = router